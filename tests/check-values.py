import subprocess

expected_values_file = open("expected-values.txt", 'r')
expected_values = [value.strip() for value in expected_values_file]
gcc_errors = open("gcc-errors.txt", 'w')

for i in range(len(expected_values)):
    subprocess.call(["gcc", "test-" + str(i) + ".c"], stderr=gcc_errors)
    value = subprocess.check_output(["./a.out"]).decode("utf-8").rstrip()
    outcome = "PASS" if value == expected_values[i] else "!!FAIL!!"
    print("Test " + str(i) + ": expected " + expected_values[i] + ", actual " + value + " ... " + outcome)